#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <QString>
#include <QObject>
#include <QProcess>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void shadowProtectCreateSnapshot();
    void starteSkript();


    void test();



private slots:
    void on_detailsButton_clicked();
    void readOutput();
    void shadowProtectDetermineSnapshotName();
    void runShadowProtectDifferenziellPrepare();
    void runShadowProtectDifferentiell();
    void deleteOldSnapshots();
    void removeHarddrive();

signals:
    void readyReadStandardOutput( );
    void finished(int , QProcess::ExitStatus );



private:
    Ui::MainWindow *ui;
    bool fertig = false;
    bool anzeigeAn = true;
    QString bla;

    QProcess *process = new QProcess;

    QString command, snapshotName, lastFull;
    QString dest = "G:\\backup\\";
    QString vshadow = "C:\\tools\\software\\ctwimage64\\vshadow64.exe ";
    QString vol = "\\?\\Volume{a41fd7c6-0000-0000-0000-102000000000} ";
    QString pathToShadowProtect = "c:\\Program Files (x86)\\StorageCraft\\ShadowProtect\\";
    QString datum;

    void setzeDatum();
    //QString holeDatum();



};

#endif // MAINWINDOW_H
