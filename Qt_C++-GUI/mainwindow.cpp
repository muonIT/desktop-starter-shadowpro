/*  This file is part of QT App Shadowpro.
 *
 *  QT App Shadowpro is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QT App Shadowpro is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QT App Shadowpro; if not,  see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QString>
#include <QObject>
#include <QFile>
#include <QDebug>
#include <windows.h>
#include <QRegExp>
#include <qregexp.h>
#include <QChar>
#include <QDate>
#include <QThread>
#include <QFileInfo>


MainWindow::~MainWindow()
{
    delete ui;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    if(anzeigeAn == true) {
         ui->detailsButton->setText("Details ausblenden");
         ui->ausgabefenster->setText("Programm startet...");
    }
}




void MainWindow::shadowProtectCreateSnapshot() {
    // Snapshot erstellen:
    command = vshadow + " -p c:";
    ui->ausgabefenster->append("\nshadowProtectCreateSnapshot: ");
    ui->ausgabefenster->append("\nErzeuge shadowProtect-Snapshot\nKommando: " + command);
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(shadowProtectDetermineSnapshotName()));
    process->start(command);
}

void MainWindow::test() {
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    command = "cd c:\\Program Files (x86)\\StorageCraft\\ShadowProtect\\";
     ui->ausgabefenster->append("\n" + command + ":\n");
     process->start(command);
     command = "dir";
      ui->ausgabefenster->append("\n" + command + ":\n");
     process->start(command);
     Sleep(1);
     command = "cd..";
      ui->ausgabefenster->append("\n" + command + ":\n");
     process->start(command);
     Sleep(1);
     command = "dir";
      ui->ausgabefenster->append("\n" + command + ":\n");
     process->start(command);
     Sleep(1);
     command = "dir 'c:\\Program Files (x86)\\StorageCraft\\ShadowProtect\\ '";
      ui->ausgabefenster->append("\n" + command + ":\n");
     process->start(command);
     Sleep(1);
}

void MainWindow::deleteOldSnapshots() {
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    command = vshadow + " -da";
    ui->ausgabefenster->append("\nLoesche alte Snapshots...\nKommando: " + command);
    process->start(command);
    if( process->waitForFinished() ) {
        ui->ausgabefenster->append("\nalles gelöscht... hoffentlich... ");
        process->terminate();
        removeHarddrive();
    }
}

void MainWindow::removeHarddrive(){
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    command = "c:\\tools\\software\\removedrive\\Win32\\RemoveDrive.exe \"muon500G\" -L";
    ui->ausgabefenster->append("\nFestplatte sicher entfernen...\n " + command);
    process->start(command);
    if( process->waitForFinished() ) {
        ui->ausgabefenster->append("\nBackup erfolgreich abgeschlossen\nSie können die Festplatte abklemmen und dieses Fenster schließen.");
        process->terminate();
    }
}

void MainWindow::runShadowProtectDifferentiell() {

    ui->ausgabefenster->append("\nrunShadowProtectDifferentiell\nErzeuge shadowProtect-Snapshot\nKommando: " + command);
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));

    //QString file = QFileInfo(pathToShadowProtect).absoluteFilePath();
    command =  "cd \"c:\\Program Files (x86)\\StorageCraft\\ShadowProtect\\";

    dest = dest.remove("\n");
    vol = vol.remove("\n");
    snapshotName = snapshotName.remove("\n");

    QString argumente;
    argumente = "sbrun \" -mdn ( sbvol -fs " + snapshotName + vol +
            " : sbset " + dest + "\\" + lastFull + " : sbcrypt -50 : sbfile -wd " + dest + "\\" + "C_VOL-" + datum + ".spi )  " ;

    QStringList arge;
    arge << argumente;

    bla = command + argumente;

    ui->ausgabefenster->append("\nStarte differenzielle Backup:\nKommando: " + bla);
    process->start(command, arge);
    if( process->waitForFinished() ) {
        ui->ausgabefenster->append("\nDifferenzielle Backup beendet... ");
        process->terminate();
        deleteOldSnapshots();
    }
}


void MainWindow::runShadowProtectDifferenziellPrepare() {
    // Signal entfernen
    //disconnect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(runShadowProtectIncrePrepare()));

    QStringList args;
    args << pathToShadowProtect;

    // wechsele in Verzeichnis von ShadowProtect
    command = "cd ";
    ui->ausgabefenster->append("\nrunShadowProtectDifferenziellPrepare\nVerzeichnis wechseln:\nKommando: " + command +args[0]);
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    process->start(command, args);
    process->waitForFinished();

    //ermittle letztes volles Backup:
    command = "PowerShell.exe -ExecutionPolicy Unrestricted  -command \"Get-ChildItem " + dest +"\\*.spf | sort LastWriteTime -Descending | select name -ExpandProperty name | select-object -First 1\" ";
    ui->ausgabefenster->append("\nermittle letztes volles Backup:\nKommando: " + command);
    process->start(command);
    if ( process->waitForFinished() ){
        // gesucht: C_VOL-27.2.2019.spf
        QString q = "";
        QString helper = ui->ausgabefenster->toPlainText();

        QRegExp rx(".*C_VOL-.*");

        QStringList list = helper.split(" ");

        for(int j = 0; j < list.length(); j++) {
            q = list[j];
            if( rx.exactMatch( q ) ) {
                lastFull = q;
            }
        }
        lastFull = lastFull.remove("\n");
        ui->ausgabefenster->append("\nLetztes volles Backup: " + lastFull);
        process->terminate();
        runShadowProtectDifferentiell();
    }
}





void MainWindow::shadowProtectDetermineSnapshotName(){
    process->terminate();
    //Snapshotname ermitteln:
    command = vshadow + " -q | Select-String -Pattern 'Shadow copy device name:' -CaseSensitive -SimpleMatch | % { $key, $VSS = $_ -split ':', 2}";

    disconnect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(shadowProtectDetermineSnapshotName()));
    ui->ausgabefenster->append("\nshadowProtectDetermineSnapshotName\nErmittle Snapshotname:\nKommando: " + command);
    process->start(command);
    if ( process->waitForFinished() ){
        // gesucht: Shadow copy device name: \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy8

        QString q = "";
        QString helper = ui->ausgabefenster->toPlainText();

        QRegExp rx(".*HarddiskVolumeShadowCopy.*");

        QStringList list = helper.split(" ");

        for(int j = 0; j < list.length(); j++) {
            q = list[j];
            if( rx.exactMatch( q ) ) {
                snapshotName = q;
               // ui->ausgabefenster->append("\n" + list[j]);
            }
        }
        ui->ausgabefenster->append("\nLetzter snapshot: " + snapshotName);
        setzeDatum();
        process->terminate();
        runShadowProtectDifferenziellPrepare();
    }
}


void MainWindow::on_detailsButton_clicked()
{
    if( anzeigeAn == true ) {
         ui->detailsButton->setText("Details anzeigen");
         ui->ausgabefenster->hide();
         anzeigeAn = false;
    }
    else  {
         ui->detailsButton->setText("Details ausblenden");
         ui->ausgabefenster->show();
         anzeigeAn = true;
    }
}

void MainWindow::readOutput()
{
    while(process->canReadLine()){
           //qDebug() << *process->readLine();
           //qDebug() <<  process->readAll();
           ui->ausgabefenster->append( process->readAll() );
     }
}


void MainWindow::setzeDatum() {
    QDate date;//
    QTime zeit;
    //$DATUM = Get-Date -Format dd.MM.yyyy-HHmm;
    datum =    date.currentDate().toString((Qt::ISODate));
    datum += zeit.currentTime().toString("HHmm");
    ui->ausgabefenster->append("\nDatum: " + datum);
}


void MainWindow::starteSkript() {
    //erster Test: soll erstmal nur die batch Datei ausführen
    command = "c:\\tools\\skripte\\shadowprotect_diff.bat";
    //command = "ping 8.8.4.4";
    ui->ausgabefenster->append("Kommando: " + command);
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
    process->start(command);
    if( process->waitForFinished() ) {
        ui->ausgabefenster->append("\nBackup erfolgreich abgeschlossen.\nSie können die Festplatte abklemmen und dieses Fenster schließen.");
        process->terminate();
    }
}
