#27.2.19 Upuetz, muon IT

#d Some Variables
$DEST="G:\backup"
$VSHADOW="C:\tools\software\ctwimage64\vshadow64.exe"
$C="\\?\Volume{a41fd7c6-0000-0000-0000-102000000000}"

#d Take VSS Snapshot of C:
& $VSHADOW -p c:

#d Determine Name of Snapshot
& $VSHADOW -q | Select-String -Pattern 'Shadow copy device name:' -CaseSensitive -SimpleMatch | % { $key, $VSS = $_ -split ':', 2};

echo "Die VSS Datei heisst $VSS";

#d Date for the name
$DATUM = Get-Date -Format dd.MM.yyyy-HHmm;

#d ShadowProtect
cd "c:\Program Files (x86)\StorageCraft\ShadowProtect\"

echo ""
echo "Jetzt ShadowProtect"
#d Find the last full image
$SOURCE=Get-ChildItem $DEST\*.spf | sort LastWriteTime -Descending | select name -ExpandProperty name | select-object -First 1

$LECKMICHAMARSCH="-mdn ( sbvol -fs $VSS $C : sbset $DEST\$SOURCE : sbcrypt -50 : sbfile -wd $DEST\C_VOL-$DATUM.spi )"
$Prms = $LECKMICHAMARSCH.Split(" ")
#d Run Shadowprotect with the values
.\sbrun $Prms

#d delete all Snapshots
echo 'y' | & $VSHADOW -da






