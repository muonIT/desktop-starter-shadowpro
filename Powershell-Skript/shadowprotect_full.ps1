#27.2.19 Upuetz

#27.2.19 Upuetz, muon IT

#d Some Variables
$DEST="G:\backup"
$VSHADOW="C:\tools\software\ctwimage64\vshadow64.exe"
$C="\\?\Volume{a41fd7c6-0000-0000-0000-102000000000}"

#d Take VSS Snapshot of C:
& $VSHADOW -p c:

#d Determine Name of Snapshot
& $VSHADOW -q | Select-String -Pattern 'Shadow copy device name:' -CaseSensitive -SimpleMatch | % { $key, $VSS = $_ -split ':', 2};

echo "Die VSS Datei heisst $VSS";

#d Date for the name
$DATUM = Get-Date -Format dd.MM.yyyy-HHmm;

#d ShadowProtect
cd "c:\Program Files (x86)\StorageCraft\ShadowProtect\"

echo ""
echo "Jetzt ShadowProtect"
$LECKMICHAMARSCH="-mdn ( sbvol -fs $VSS $C : sbcrypt -50 : sbfile -wd $DEST\C_VOL-$DATUM.spf )"
$Prms = $LECKMICHAMARSCH.Split(" ")
.\sbrun $Prms

#d delete Snapshots
echo 'y' | & $VSHADOW -da

